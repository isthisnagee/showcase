// get a cross-browser function for adding events

let UI = {
  // TODO
  docItem: {
    marginRight: "0.5rem",
    marginTop: "0.2rem",
    padding: "0.25rem",
    display: "inline-block",
    boxShadow: "1px 1px 1px black",
    cursor: "pointer",
  },
};

function addStyle(node, style) {
  if (style instanceof String) {
    node.style = style;
  } else {
    Object.keys(style).map(k => (node.style[k] = style[k]));
  }
  return node;
}

var on = (function() {
  if (window.addEventListener) {
    return function(target, type, listener) {
      target.addEventListener(type, listener, false);
      return target;
    };
  } else {
    return function(object, sEvent, fpNotify) {
      object.attachEvent("on" + sEvent, fpNotify);
      return object;
    };
  }
})();

function clickedInside(outer, inner, yes, no) {
  on(outer, "click", function(e) {
    if (e.target != inner) {
      no();
    } else {
      yes();
    }
  });
}

window.onload = function() {
  // USE A TREE TO MANAGE REDRAWS DUH
  let h = HTML();
  let dock = Dock();
  let desktop = DesktopGrid();

  let fileExplorerModal = Modal(
    h.e("div", {})([
      "FILE EXPLORER",
      h.e("ul", {})(
        [
          h.e("li", {})("FILE 1"),
          h.e("li", {})("FILE 2"),
          h.e("li", {})("FILE 3"),
          h.e("li", {})("FILE 4"),
          h.e("li", {})("FILE 5"),
        ],
        {},
      ),
    ]),
  );

  desktop.AddIcon(
    7,
    3,
    "http://thewindowsclub.thewindowsclubco.netdna-cdn.com/wp-content/uploads/2010/08/Windows-Explorer-Icon.png",
    "icon",
    {
      leftClick: () => fileExplorerModal.toggle(),
      rightClick: e => {
        e.preventDefault();
        const menuItem = text => {
          const item = h.e("div", { style: `padding: 3px; border-bottom: 1px solid black` })(text);
          on(item, "mouseenter", e => (item.style.backgroundColor = "rgba(0,0,0,.1)"));
          on(item, "mouseleave", e => (item.style.backgroundColor = "transparent"));
          return item;
        };

        let menu = h.e("div", {
          style: `
              left: ${e.clientX}px;
              top: ${e.clientY}px;
              background-color: #faebd7;
              position: fixed;
              z-index: 1;
              height: 200px;
              width: 100px;
              padding: 0; margin: 0;
          `,
        })(
          h.e("div", {})(
            [
              menuItem("item 0"),
              menuItem("item 1"),
              menuItem("item 2"),
              menuItem("item 3"),
              menuItem("item 4"),
            ],
            {},
          ),
        );
        document.body.appendChild(menu);
        on(menu, "mouseleave", e => document.body.removeChild(menu));
        return false;
      },
    },
  );

  dock.AddItem("say hi", () => fileExplorerModal.toggle());
  dock.AddItem("bg color", () => {
    const desktop = document.getElementById("desktop");
    const curr = desktop.style.backgroundColor;
    if (curr === "red") desktop.style.backgroundColor = "#000084";
    else desktop.style.backgroundColor = "red";
  });
  dock.Draw();
};

function DesktopGrid() {
  const CSS_WIdTH = 40;
  const desktopGrid = document.getElementById("desktop-grid");
  // an increasing id used to store elements.
  let _callbackId = 0;
  const _makeCallbackId = () => (_callbackId += 1);
  // used to store functions to be accessed on click
  let callbacks = [];

  on(desktopGrid, "dragover", e => e.preventDefault());
  on(desktopGrid, "drop", e => {
    e.preventDefault();
    const xPx = e.clientX + CSS_WIdTH;
    const yPx = e.clientY + CSS_WIdTH;
    const oldId = e.dataTransfer.getData("old-position-id");
    const imgSrc = e.dataTransfer.getData("img-src");
    const imgAlt = e.dataTransfer.getData("img-alt");
    const leftClickId = e.dataTransfer.getData("leftclick-id");
    const rightClickId = e.dataTransfer.getData("rightclick-id");
    RemoveIconId(oldId);
    let leftClick = callbacks.find(c => c.id == leftClickId);
    let rightClick = callbacks.find(c => c.id == rightClickId);
    if (leftClick) leftClick = leftClick.fn;
    else leftClick = { fn: null, id: -1 };
    if (rightClick) rightClick = rightClick.fn;
    else rightClick = { fn: null, id: -1 };
    AddIconPx(xPx, yPx, imgSrc, imgAlt, { rightClick, leftClick });
  });

  function makeIconDraggable(x, y, imgSrc, imgAlt, { leftClickName, rightClickName }, icon) {
    on(icon, "dragstart", e => {
      e.dataTransfer.setData("old-position-id", `icon-${x}-${y}`);
      e.dataTransfer.setData("img-src", imgSrc);
      e.dataTransfer.setData("img-alt", imgAlt);
      e.dataTransfer.setData("leftclick-id", leftClickName);
      e.dataTransfer.setData("rightclick-id", rightClickName);
    });
    return icon;
  }

  const getCurrentSize = () => ({
    width: desktopGrid.clientWidth,
    height: desktopGrid.clientHeight,
  });

  function GetDimensions() {
    const pxSize = getCurrentSize();
    return {
      height: Math.floor(pxSize.height / CSS_WIdTH),
      width: Math.floor(pxSize.width / CSS_WIdTH),
    };
  }

  function RemoveIcon(x, y) {
    x = Math.floor(x);
    y = Math.floor(y);
    const id = `icon-${x}-${y}`;
    // check if this icon exists
    const iconExists = document.getElementById(id);
    if (iconExists) desktopGrid.removeChild(iconExists);
  }

  function RemoveIconId(id) {
    const validIdRegex = /icon-\d+-\d+/;
    const validId = validIdRegex.test(id);
    if (!validId) return false;

    const iconExists = document.getElementById(id);
    if (iconExists) {
      desktopGrid.removeChild(iconExists);
      return true;
    } else return false;
  }

  function AddIcon(x, y, src, alt, { rightClick, leftClick }) {
    x = Math.floor(x);
    y = Math.floor(y);
    const id = `icon-${x}-${y}`;
    RemoveIcon(x, y);
    const currDimension = GetDimensions();
    const validCoord = (c, p) => c >= 0 && c <= currDimension[p];

    // are the dimensions possible:
    const validPosition = validCoord(x, "width") && validCoord(y, "height");
    if (!validPosition) return;
    const rightClickName = _makeCallbackId();
    callbacks.push({
      fn: rightClick,
      id: rightClickName,
    });
    const leftClickName = _makeCallbackId();
    callbacks.push({
      fn: leftClick,
      id: leftClickName,
    });

    const divStyle = `
        grid-column: ${x};
        grid-row: ${y};
    `;
    const imgStyle = `
        max-width: 100%;
        max-height: 100%;
    `;
    const h = HTML();
    let icon = h.e("div", { id, style: divStyle, draggable: true })(
      h.e("img", { style: imgStyle, src, alt })(),
    );

    on(icon, "click", leftClick);
    on(icon, "contextmenu", e => {
      rightClick(e);
      return false;
    });
    icon = makeIconDraggable(x, y, src, alt, { rightClickName, leftClickName }, icon);
    desktopGrid.appendChild(icon);
    return icon;
  }

  function AddIconPx(pxX, pxY, src, alt, callbacks) {
    return AddIcon(Math.floor(pxX / CSS_WIdTH), Math.floor(pxY / CSS_WIdTH), src, alt, callbacks);
  }

  return {
    GetDimensions,
    AddIcon,
    AddIconPx,
    RemoveIcon,
  };
}

function Modal(html) {
  let modalOpen = false;

  const modalContainerStyle = `
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.1); /* Black w/ opacity */
  `;

  const modalCloseStyle = `
    cursor: pointer;
    float: right;
    font-size: 28px;
    font-weight: bold;
  `;

  // find a way to center
  const modalContentStyle = `
    box-shadow: 2px 2px 2px black;
    background-color: #faebd7;
    margin: auto; /* 15% from the top and centered */
    padding: 20px;
    border: 6px solid gray;
    height: 80vh;
    width: 80%; /* Could be more or less, depending on screen size */
  `;

  let h = HTML();
  let containerDiv = h.e("div", { style: modalContainerStyle });
  let contentWrapper = h.e("div", { style: modalContentStyle });
  let closeContent = h.e("div", { style: modalCloseStyle })("x");
  let content = contentWrapper([closeContent, html]);
  let container = containerDiv(content);

  const toggle = () => {
    modalOpen = !modalOpen;
    container.style.display = modalOpen ? "block" : "none";
  };
  const close = () => {
    container.style.display = "none";
    modalOpen = false;
  };
  on(closeContent, "click", () => close());
  clickedInside(container, content, yes => false, no => close());
  document.body.appendChild(container);
  return {
    toggle,
  };
}

function Dock() {
  const dockItem = (name, callback) => {
    let item = document.createElement("div");
    addStyle(item, UI.docItem);
    on(item, "click", callback);
    item.innerHTML = name;
    return { html: item, name };
  };

  const dock = document.getElementById("dock");
  // state for dock
  let dockItems = [];

  const Draw = () => {
    dock.innerHTML = "";
    dockItems.forEach(item => dock.appendChild(item.html));
  };

  const AddItem = (name, callback) => {
    const newItem = dockItem(name, callback);
    // we don't want repeat items
    const nameAlreadyExists = dockItems.findIndex(item => item.name === name);
    if (nameAlreadyExists >= 0) dockItems[nameAlreadyExists] = newItem;
    else dockItems.push(newItem);
  };

  const RemoveItem = name => {
    const newDockItems = dockItems.filter(item => item.name !== name);
    dockItem = newDockItems;
  };
  const AddItemAndDraw = (name, callback) => {
    AddItem(name, callback);
    Draw();
  };

  return {
    AddItem,
    AddItemAndDraw,
    RemoveItem,
    Draw,
  };
}

function HTML() {
  function htmlCompatible(attrs) {
    const fixKey = str => {
      if (str === "className") return "class";
      return str
        .split("")
        .map(c => (c.toUpperCase() === c ? `-${c.toLowerCase()}` : c))
        .join("");
    };
    let compatibleAttrs = {};
    Object.keys(attrs).forEach(k => {
      compatibleAttrs[fixKey(k)] = attrs[k];
    });
    return compatibleAttrs;
  }

  function e(name, attrs) {
    const childNode = (p, n) => {
      if (n instanceof Node) p.appendChild(n);
      else if (n instanceof Function) {
        try {
          p.appendChild(n());
        } catch (e) {
          /* handle error */
          console.log(`tried to append ${n} to ${p}`);
        }
      } else
        try {
          p.appendChild(n);
        } catch (e) {
          p.innerHTML = n;
        }
    };
    return (val, moreAttrs) => {
      const finalAttrs = Object.assign({}, attrs, moreAttrs);
      const attributes = finalAttrs;
      let node = document.createElement(name);
      Object.assign(node, finalAttrs);
      if (val instanceof Array) val.forEach(v => childNode(node, v));
      else childNode(node, val);
      return node;
    };
  }

  return {
    e,
  };
}
